const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function kata1() {
    document.write("<h1>Kata 1</h1>");
    let newElement = document.createElement("div");
    let result = gotCitiesCSV.split(",")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result; 
}
kata1();

function kata2() {
    document.write("<h1>Kata 2</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.split(" ");
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata2();


function kata3() {
    document.write("<h1>Kata 3</h1>");
    let newElement = document.createElement("div");
    let array = gotCitiesCSV.split(",")
    let string = array.toString()
    let result = string.replace(/,/g, ";")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result; 
}
kata3();

function kata4(){
    document.write("<h1>Kata 4</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.toString();
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata4();


function kata5(){
    document.write("<h1>Kata 5</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.slice(0,5);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata5();


function kata6(){
    document.write("<h1>Kata 6</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.slice(3);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata6();


function kata7(){
    document.write("<h1>Kata 7</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.slice(2,5);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata7();

function kata8(){
    document.write("<h1>Kata 8</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(2, 1);
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata8();


function kata9(){
    document.write("<h1>Kata 9</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(5, 3);
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata9();


function kata10(){
    document.write("<h1>Kata 10</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(2, 0, "Rohan")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata10();

function kata11(){
    document.write("<h1>Kata 11</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.splice(5, 1, "Deadest Marshes")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata11();

function kata12(){
    document.write("<h1>Kata 12</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.slice(0, 14);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata12();

function kata13(){
    document.write("<h1>Kata 13</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.slice(69);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata13();

function kata14(){
    document.write("<h1>Kata 14</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.slice(23, 39);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata14();

function kata15(){
    document.write("<h1>Kata 15</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.substring(69);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata15();


function kata16(){
    document.write("<h1>Kata 16</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.substring(23, 39);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata16();

function kata17(){
    document.write("<h1>Kata 17</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.indexOf("only")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata17();

function kata18(){
    document.write("<h1>Kata 18</h1>");
    let newElement = document.createElement("div");
    let result = bestThing.lastIndexOf("bit")
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata18();

function kata19(){
    document.write("<h1>Kata 19</h1>");
    let newElement = document.createElement("div");
    let array = gotCitiesCSV.split(",");
    let pattern = 'ee';
    let pattern2 =  'aa';
    let ee = array.filter(function (str) { return str.includes(pattern); });
    let aa = array.filter(function (str) { return str.includes(pattern2); });
    let result = ee.concat(aa);
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata19();


function kata20(){
    document.write("<h1>Kata 20</h1>");
    let newElement = document.createElement("div");
    let pattern = 'or';
    let result = lotrCitiesArray.filter(function (str) { return str.includes(pattern)});
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata20();

function kata21(){
    document.write("<h1>Kata 21</h1>");
    let newElement = document.createElement("div");
    let array = bestThing.split(" ")
    let result = array.filter(function (str) {return str.startsWith("b")})
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata21();

function kata22(){
    document.write("<h1>Kata 22</h1>");
    let newElement = document.createElement("div");
    if (lotrCitiesArray.includes("Mirkwood")){
        result = "Yes"
    }
    else {
        result = "No"
    }
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata22();

function kata23(){
    document.write("<h1>Kata 23</h1>");
    let newElement = document.createElement("div");
    if (lotrCitiesArray.includes("Hollywood")){
        result = "Yes"
    }
    else {
        result = "No"
    }
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata23();


function kata24(){
    document.write("<h1>Kata 24</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.indexOf("Mirkwood");
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata24();

function kata25(){
    document.write("<h1>Kata 25</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.filter(function (str) { return str.includes(" ")});
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata25();

function kata26(){
    document.write("<h1>Kata 26</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.reverse();
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata26();

function kata27(){
    document.write("<h1>Kata 27</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.sort();
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata27();

function kata28(){
    document.write("<h1>Kata 28</h1>");
    let newElement = document.createElement("div");
    let result = lotrCitiesArray.sort(function(a, b){
        return b.length - a.length
    });
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata28();

function kata29(){
    document.write("<h1>Kata 29</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.pop()
    let result = lotrCitiesArray
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata29();

function kata30(){
    document.write("<h1>Kata 30</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.push("Rohan")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata30();

function kata31(){
    document.write("<h1>Kata 31</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.shift()
    let result = lotrCitiesArray
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata31();

function kata32(){
    document.write("<h1>Kata 32</h1>");
    let newElement = document.createElement("div");
    lotrCitiesArray.unshift("Deadest Marshes")
    let result = lotrCitiesArray;
    newElement.textContent = result;
    document.body.appendChild(newElement);
    return result;
}
kata32();
